package Helper;

/**
 * Created by Daniel-PC on 16.06.2017.
 */

public final class StringParser {

    public final static double toDouble(String string) {
        return Double.parseDouble(string);
    }

    public final static String toString(Double value) {
        return String.valueOf(value);
    }
}
