package DomainModel;

/**
 * Created by Daniel-PC on 16.06.2017.
 */

public class Calculator {

    public double lastCalculation;

    public Calculator() {
        this.lastCalculation = 0;
    }

    private double add(double x, double y) {
        lastCalculation = x + y;
        return lastCalculation;
    }

    private double sub(double x, double y) {
        lastCalculation = x - y;
        return lastCalculation;
    }

    private double mul(double x, double y) {
        lastCalculation = x * y;
        return lastCalculation;
    }

    private double div(double x, double y) {
        lastCalculation = x / y;
        return lastCalculation;
    }

    public double calc(double x, double y, Operator op) {

        if (op == Operator.PLUS) return add(x, y);
        if (op == Operator.MINUS) return sub(x, y);
        if (op == Operator.TIMES) return mul(x, y);
        if (op == Operator.OBELUS) return div(x, y);

        //should never happen
        if (op == Operator.EMPTY) return 0;
        return 0;
    }
}
