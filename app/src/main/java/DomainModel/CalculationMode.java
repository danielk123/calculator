package DomainModel;

/**
 * Created by Daniel-PC on 17.06.2017.
 */

public enum CalculationMode {
    FINISHED,
    CALCULATION,
    STARTED
}
