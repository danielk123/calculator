package DomainModel;

/**
 * Created by Daniel-PC on 16.06.2017.
 */

public enum Operator {
    EMPTY,
    PLUS,
    MINUS,
    TIMES,
    OBELUS
}
