package kopfdaniel.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import DomainModel.CalculationMode;
import DomainModel.Calculator;
import DomainModel.Operator;
import Helper.StringParser;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String EMPTY_DISPLAY_VALUE = "0";
    private Button AddButton;
    private Button SubButton;
    private Button MulButton;
    private Button DivButton;
    private Button ZeroButton;
    private Button OneButton;
    private Button TwoButton;
    private Button ThreeButton;
    private Button FourButton;
    private Button FiveButton;
    private Button SixButton;
    private Button SevenButton;
    private Button EightButton;
    private Button NineButton;
    private Button CalcButtn;
    private Button CommaButton;
    private Button ClearButton;
    private TextView DisplayField;
    private TextView OperatorField;
    private Calculator calculator;
    private Operator operator;
    private boolean resetWithAppend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        initializeButtons();
        DisplayField = (TextView) findViewById(R.id.DisplayField);
        DisplayField.setText(EMPTY_DISPLAY_VALUE);
        OperatorField = (TextView) findViewById(R.id.OperatorField);
        calculator = new Calculator();
        setOperator(Operator.EMPTY);
        resetWithAppend = false;
    }

    private void initializeButtons() {

        AddButton = (Button) findViewById(R.id.buttonAdd);
        AddButton.setOnClickListener(this);

        SubButton = (Button) findViewById(R.id.buttonSub);
        SubButton.setOnClickListener(this);

        MulButton = (Button) findViewById(R.id.buttonDiv);
        MulButton.setOnClickListener(this);

        DivButton = (Button) findViewById(R.id.ButtonMul);
        DivButton.setOnClickListener(this);

        ZeroButton = (Button) findViewById(R.id.ButtonZero);
        ZeroButton.setOnClickListener(this);

        OneButton = (Button) findViewById(R.id.buttonOne);
        OneButton.setOnClickListener(this);

        TwoButton = (Button) findViewById(R.id.buttonTwo);
        TwoButton.setOnClickListener(this);

        ThreeButton = (Button) findViewById(R.id.buttonThree);
        ThreeButton.setOnClickListener(this);

        FourButton = (Button) findViewById(R.id.ButtonFour);
        FourButton.setOnClickListener(this);

        FiveButton = (Button) findViewById(R.id.ButtonFive);
        FiveButton.setOnClickListener(this);

        SixButton = (Button) findViewById(R.id.buttonSix);
        SixButton.setOnClickListener(this);

        SevenButton = (Button) findViewById(R.id.buttonSeven);
        SevenButton.setOnClickListener(this);

        EightButton = (Button) findViewById(R.id.buttonEight);
        EightButton.setOnClickListener(this);

        NineButton = (Button) findViewById(R.id.ButtonNine);
        NineButton.setOnClickListener(this);

        CalcButtn = (Button) findViewById((R.id.buttonCalc));
        CalcButtn.setOnClickListener(this);

        CommaButton = (Button) findViewById((R.id.ButtonComma));
        CommaButton.setOnClickListener(this);

        ClearButton = (Button) findViewById((R.id.ButtonClear));
        ClearButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.buttonOne:
                appendDisplayText("1");
                break;

            case R.id.buttonTwo:
                appendDisplayText("2");
                break;

            case R.id.buttonThree:
                appendDisplayText("3");
                break;
            case R.id.ButtonFour:
                appendDisplayText("4");
                break;

            case R.id.ButtonFive:
                appendDisplayText("5");
                break;

            case R.id.buttonSix:
                appendDisplayText("6");
                break;

            case R.id.buttonSeven:
                appendDisplayText("7");
                break;

            case R.id.buttonEight:
                appendDisplayText("8");
                break;

            case R.id.ButtonNine:
                appendDisplayText("9");
                break;

            case R.id.ButtonZero:
                appendDisplayText("0");
                break;

            case R.id.buttonAdd:
                add();
                break;

            case R.id.buttonSub:
                sub();
                break;

            case R.id.ButtonMul:
                mul();
                break;

            case R.id.buttonDiv:
                div();
                break;

            case R.id.buttonCalc:
                calc();
                break;
            case R.id.ButtonComma:
                appendDisplayText(".");
                break;

            case R.id.ButtonClear:
                clear();
                break;
            default:
                break;
        }
    }

    private void calc() {
        if (operator == Operator.EMPTY) {
            return;
        }

        double result = calculator.calc(calculator.lastCalculation, StringParser.toDouble(DisplayField.getText().toString()), operator);

        setOperator(Operator.EMPTY);
        calculator.lastCalculation = 0;
        resetWithAppend = true;
        updateDisplay(StringParser.toString(result));
    }

    private void add() {
        if (operator == Operator.EMPTY) {
            setOperator(Operator.PLUS);
            setMode(CalculationMode.STARTED);
            return;
        }

        if (!getValidatedOldDisplayText().equals(EMPTY_DISPLAY_VALUE)) {
            calculator.calc(calculator.lastCalculation, StringParser.toDouble(DisplayField.getText().toString()), operator);
            setMode(CalculationMode.CALCULATION);
            setOperator(Operator.PLUS);
            return;
        }

        changeOperatorWhileInCalculationMode(Operator.PLUS);
    }

    private void sub() {
        if (operator == Operator.EMPTY) {
            setOperator(Operator.MINUS);
            setMode(CalculationMode.STARTED);
            return;
        }

        if (!getValidatedOldDisplayText().equals(EMPTY_DISPLAY_VALUE)) {
            calculator.calc(calculator.lastCalculation, StringParser.toDouble(DisplayField.getText().toString()), operator);
            setMode(CalculationMode.CALCULATION);
            setOperator(Operator.MINUS);
            return;
        }

        changeOperatorWhileInCalculationMode(Operator.MINUS);
    }

    private void mul() {
        if (operator == Operator.EMPTY) {
            setOperator(Operator.TIMES);
            setMode(CalculationMode.STARTED);
            return;
        }

        if (!getValidatedOldDisplayText().equals(EMPTY_DISPLAY_VALUE)) {
            calculator.calc(calculator.lastCalculation, StringParser.toDouble(DisplayField.getText().toString()), operator);
            setMode(CalculationMode.CALCULATION);
            setOperator(Operator.TIMES);
            return;
        }

        changeOperatorWhileInCalculationMode(Operator.TIMES);
    }

    private void div() {
        if (operator == Operator.EMPTY) {
            setOperator(Operator.OBELUS);
            setMode(CalculationMode.STARTED);
            return;
        }

        if (!getValidatedOldDisplayText().equals(EMPTY_DISPLAY_VALUE)) {
            calculator.calc(calculator.lastCalculation, StringParser.toDouble(DisplayField.getText().toString()), operator);
            setMode(CalculationMode.CALCULATION);
            setOperator(Operator.OBELUS);
            return;
        }

        changeOperatorWhileInCalculationMode(Operator.OBELUS);
    }

    private void appendDisplayText(String buttonInput) {

        String oldDisplayText = getValidatedOldDisplayText();
        if (oldDisplayText.contains(".") && buttonInput.equals(".")) return;

        String newDisplayText = oldDisplayText.concat(buttonInput);
        updateDisplay(newDisplayText);
    }

    private void clear() {
        calculator.lastCalculation = 0;
        setOperator((Operator.EMPTY));
        updateDisplay(EMPTY_DISPLAY_VALUE);
    }

    private void updateDisplay(String text) {
        if (text.endsWith(".0")) text = text.replace(".0", "");
        while (text.startsWith("0") && !text.startsWith("0.") && text.length() > 1) {
            text = text.substring(1);
        }
        DisplayField.setText(text);
    }

    private String getValidatedOldDisplayText() {

        String oldText;
        if (resetWithAppend) {
            oldText = EMPTY_DISPLAY_VALUE;
            resetWithAppend = false;
        } else oldText = DisplayField.getText().toString();

        return oldText;
    }


    private void setMode(CalculationMode mode) {

        if (mode == CalculationMode.FINISHED) {
            operator = Operator.EMPTY;
            resetWithAppend = true;
            //updateDisplay(StringParser.toString(calculator.lastCalculation));
        }
        if (mode == CalculationMode.STARTED) {
            calculator.lastCalculation = StringParser.toDouble(DisplayField.getText().toString());
            resetWithAppend = true;
        }

        if (mode == CalculationMode.CALCULATION) {
            resetWithAppend = true;
            updateDisplay(StringParser.toString(calculator.lastCalculation));
        }
    }

    private void changeOperatorWhileInCalculationMode(Operator op) {
        if (operator == Operator.EMPTY) return;

        setOperator(op);
        setMode(CalculationMode.CALCULATION);
    }

    private void setOperator(Operator op) {
        operator = op;
        if (op == Operator.PLUS)
            OperatorField.setText("+");
        if (op == Operator.MINUS)
            OperatorField.setText("-");
        if (op == Operator.TIMES)
            OperatorField.setText("*");
        if (op == Operator.OBELUS)
            OperatorField.setText("/");
        if (op == Operator.EMPTY)
            OperatorField.setText("");
    }
}


